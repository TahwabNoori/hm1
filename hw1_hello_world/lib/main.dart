import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',

      home: Scaffold(
        appBar: AppBar(
          title: Text("Tahwab Noori, Fall 2021"),
        ),
        body: Center(
          child: Text('\t“Your mind is like this water, my friend.\n When it is agitated, it becomes difficult to see.\n But if you allow it to settle, the answer becomes clear.”\n\n\t-Master Oogway'),
        ),
      )
    );
  }
}




